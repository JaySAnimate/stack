﻿using System;
using System.Collections.Generic;

namespace Stack
{
    class Stack<T> where T : IComparable
    {
        T[] StackArray;
        int Current = -1;
        private Dictionary<int, T> ForMax;

        public Stack(uint Capacity)
        {
            if (Capacity <= 0)
                throw new Exception("The Capacity must be greather than 0");
            StackArray = new T[Capacity];
            ForMax = new Dictionary<int, T>();
        }
        public bool IsFull() { return Current == StackArray.Length - 1; }
        public bool IsEmpty() { return Current == -1; }
        public void Push(T item)
        {
            if (IsFull())
                throw new Exception("Stack is Full");
            if (IsEmpty())
            {
                StackArray[++Current] = item;
                ForMax[Current] = Top();
                return;
            }
            if (item.CompareTo(ForMax[Current]) > 0)
            {
                StackArray[++Current] = item;
                ForMax[Current] = item;
                return;
            }
            ForMax[Current + 1] = ForMax[Current];
            StackArray[++Current] = item;
        }
        public void PushToBottom(T item)
        {
            if (IsEmpty())
                Push(item);
            else
            {
                T tempItem = Pop();
                PushToBottom(item);
                Push(tempItem);
            }
        }
        public T Pop()
        {
            if (IsEmpty())
                throw new Exception("Stack is Empty");
            ForMax.Remove(Current);
            return StackArray[Current--];
        }
        public T Top()
        {
            if (IsEmpty())
                throw new Exception("Stack is Empty");
            return StackArray[Current];
        }
        public T Max()
        {
            if (IsEmpty())
                throw new Exception("Stack cannot have a maximum element because it is empty :)");
            return ForMax[Current];
        }
    }
}