﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Stack<int> stack = new Stack<int>(10);

                stack.Push(150);
                stack.Push(84);
                stack.Push(9);
                stack.Push(500);
                stack.Push(0); 

                Console.WriteLine(stack.Max());
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
